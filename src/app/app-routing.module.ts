import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './theme/layout/admin/admin.component';
import { UserComponent } from './theme/layout/user/user.component';

const routes: Routes = [
  // {
  //   path: 'admin',
  //   component: AdminComponent,
  //   children: [
  //     {
  //       path: '',
  //       redirectTo: 'sample-page',
  //       pathMatch: 'full'
  //     },
  //     {
  //       path: 'sample-page',
  //       loadChildren: () => import('./demo/pages/sample-page/sample-page.module').then(module => module.SamplePageModule)
  //     }
  //   ]
  // },
  {
    path: 'admin',
    component: AdminComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./admin-panel/admin-panel.module').then(module => module.AdminPanelModule)
      }
    ]
  },
  {
    path: 'user',
    component: UserComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./user-panel/user-panel.module').then(module => module.UserPanelModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
