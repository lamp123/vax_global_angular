import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SamplePageComponent} from './sample-page.component';
import { HomeComponent } from '../../../user-panel/home/home.component';

const routes: Routes = [
  {
    path: '',
    component: SamplePageComponent
  },
  {
    path: 'another',
    component: HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SamplePageRoutingModule { }
