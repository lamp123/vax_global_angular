<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

    protected $primaryKey = 'countries_id';

    protected $table = 'countries';

    protected $fillable = [
        'country_name',
    ];
}
