<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'states';

    protected $primaryKey = 'states_id';

    protected $fillable = [
        'state_name','country_id',
    ];
}
