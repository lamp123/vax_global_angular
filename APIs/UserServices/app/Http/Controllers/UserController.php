<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Crypt;
use App\Models\User;

class UserController extends Controller
{
    public function userList()
    {
        $user = User::where('enable','=','1')->get();

        return $this->jsonResponse(['user' => $user], 1, 'User List');
    }

    public function addUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|string|unique:users|email',
            'phone_no' =>'required',
            'role' =>'required',
            'password' => 'required|min:8',
        ]);

        if ($validator->fails()) {
            $errors = $this->errorsArray($validator->errors()->toArray());
            //dd($errors);            
            return $this->jsonResponse([], 0, implode(",", $errors));
        }
        $input = $request->all();
        $input['password'] = Crypt::encrypt($input['password']);
        $user = User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'phone_no' => $input['phone_no'],
            'role' => $input['role'],
            'password' => $input['password'],
        ]);
        return $this->jsonResponse(['user' => $user], 1, 'User Successfully Added');
    }

    public function editUser(Request $request, $id)
    {   
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|string|email',
            'phone_no' =>'required',
            'role' =>'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $this->errorsArray($validator->errors()->toArray());
            //dd($errors);            
            return $this->jsonResponse([], 0, implode(",", $errors));
        }

        $model = 'App\\Models\\User'; 
        $user = $model::find($id); 
         if($user)
        {
            $input = $request->all();
            $input['password'] = Crypt::encrypt($input['password']);
           
            $user->name =  $input['name'];
            $user->email = $input['email'];
            $user->phone_no = $input['phone_no'];
            $user->role = $input['role'];
            $user->password = $input['password'];

            $user->save();

            $data =  $user;

            return $this->jsonResponse(['user' => $data], 1, 'User Successfully Updated');
        }

        return $this->jsonResponse([], 2, 'User Not Found');
    }

    public function deleteUser($id)
    {
        $model = 'App\\Models\\User'; 
        $data = $model::find($id); 
        if($data){
            $data->enable = 0;
            $data->save();
            return $this->jsonResponse([],1, 'User Successfully deleted');
        }

        return $this->jsonResponse([],2, 'User Not Found');
    }
    
}
