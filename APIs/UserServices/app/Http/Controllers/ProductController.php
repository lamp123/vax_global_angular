<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Validator;

class ProductController extends Controller
{
    public function productList()
    {
        $product = Product::where('enable','=','1')->get();

        $products =  $product;
        return $this->jsonResponse(['products' => $products], 1, 'Product List');
    }

    public function addProduct(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'amount' =>'required',
        ]);

        if ($validator->fails()) {
            $errors = $this->errorsArray($validator->errors()->toArray());
            //dd($errors);            
            return $this->jsonResponse([], 0, implode(",", $errors));
        }
        $input = $request->all();
        $product = Product::create([
            'title' => $input['title'],
            'description' => $input['description'],
            'amount' => $input['amount'],
        ]);
        $data =  $product;
        return $this->jsonResponse(['product' => $data], 1, 'Product Successfully Added');
    }

    public function editProduct(Request $request, $id)
    {   
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'amount' =>'required',
        ]);

        if ($validator->fails()) {
            $errors = $this->errorsArray($validator->errors()->toArray());
            //dd($errors);            
            return $this->jsonResponse([], 0, implode(",", $errors));
        }   

        $model = 'App\\Models\\Product'; 
        $product = $model::find($id); 
         if($product)
        {
            $input = $request->all();
           
            $product->title =  $input['title'];
            $product->description = $input['description'];
            $product->amount = $input['amount'];

            $product->save();

            return $this->jsonResponse(['product' => $product], 1, 'Product Successfully Updated');
        }

        return $this->jsonResponse([], 2, 'Product Not Found');
    }

    public function deleteProduct($id)
    {
        $model = 'App\\Models\\Product'; 
        $data = $model::find($id); 
        if($data){
            $data->enable = 0;
            $data->save();
            return $this->jsonResponse([],1, 'Product Successfully deleted');
        }

        return $this->jsonResponse([],2, 'Product Not Found');
    }
}
