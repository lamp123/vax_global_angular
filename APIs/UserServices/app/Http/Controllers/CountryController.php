<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\State;
use App\Models\Country;
use DB;

class CountryController extends Controller
{
    public function countryList(Request $request)
    {
        $list = Country::all();

        $data = array(
            'countries' => $list,
        );

        return $this->jsonResponse(['data' => $data], 1, 'countries List');
    }

    public function stateList(Request $request, $country_id)
    {

        $list = DB::table('states')->select('states_id','country_id','state_name','country_name')
        ->join('countries','countries.countries_id','=','states.country_id')->where('country_id','=',$country_id)->get();

        $data = array(
            'states' => $list,
        );
        
        return $this->jsonResponse(['data' => $data], 1, 'states List');
    }

    public function cityList(Request $request, $state_id)
    {

        $list = DB::table('cities')->select('city_id','state_id','state_name','city_name')
        ->join('states','states.states_id','=','cities.state_id')->where('state_id','=',$state_id)->get();

        $data = array(
            'cities' => $list,
        );
        
        return $this->jsonResponse(['data' => $data], 1, 'cities List');
    }
}
