<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    
    public $successStatus = 200;
    public $data, $to;

    public function jsonResponse($data, $status, $msg = ''){
        if($status == 0){
            return response()->json(
            [
                'success' => false,
                'status_code' => 301,
                'message' => $msg,
                'data' => $data
            ], $this->successStatus); 
        }
        else if($status == 1){
            return response()->json(
            [
                'success' => true,
                'status_code' => 200,
                'message' => $msg,
                'data' => $data
            ], $this->successStatus); 
        }
        else if($status == 2){
            return response()->json(
            [
                'success' => false,
                'status_code' => 301,
                'message' => $msg
            ], $this->successStatus); 
        }
    }

    public function errorsArray($errors){
        $e = array();
        foreach ($errors as $key => $value) {
            //dump($value);
            foreach ($value as $k => $v) {
                array_push($e, $v);
            }
        }
        return $e;
    }
}
