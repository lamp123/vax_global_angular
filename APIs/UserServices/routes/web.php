<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
//User Crud Routes
$router->get('/user-list', 'UserController@userList');
$router->post('/add-user', 'UserController@addUser');
$router->put('/edit-user/{id}','UserController@editUser');
$router->delete('/delete-user/{id}','UserController@deleteUser');

//Product Crud Routes
$router->get('/product-list', 'ProductController@productList');
$router->post('/add-product', 'ProductController@addProduct');
$router->put('/edit-product/{id}','ProductController@editProduct');
$router->delete('/delete-product/{id}','ProductController@deleteProduct');

//Country State Api's
$router->get('/country-list','CountryController@countryList');
$router->get('/state-list/{country_id}','CountryController@stateList');
$router->get('/city-list/{state_id}','CountryController@cityList');