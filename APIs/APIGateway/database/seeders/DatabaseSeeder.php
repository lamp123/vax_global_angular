<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UsersTableSeeder');
        DB::table('users')->insert([
            [
                'email' => 'admin@itaims.com',
                'role' => 'Admin',
                'password' => Hash::make('123456'),
               
            ],
            [
                'email' => 'ajay@itaims.com',
                'role' => 'User',
                'password' => Hash::make('123456')
               
            ]
        ]);
    }
}
