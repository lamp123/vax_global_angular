<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class LocationServices
{
    use ConsumeExternalService;

    /**
     * The base uri to consume authors service
     * @var string
     */
    public $baseUri;

    /**
     * Authorization secret to pass to book api
     * @var string
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = env('USERS_SERVICE_BASE_URL');
        $this->secret = env('USERS_SERVICE_SECRET');

        
    }

    public function listCountry($data){
        return $this->performRequest('GET', '/country-list', $data);
    }

    public function listState($data){
        return $this->performRequest('GET', '/state-list', $data);
    }    
    
    public function listCity($data){
        return $this->performRequest('GET', '/city-list', $data);
    }

}