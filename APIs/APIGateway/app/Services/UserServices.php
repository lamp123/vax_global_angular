<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class UserServices
{
    use ConsumeExternalService;

    /**
     * The base uri to consume authors service
     * @var string
     */
    public $baseUri;

    /**
     * Authorization secret to pass to book api
     * @var string
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = env('USERS_SERVICE_BASE_URL');
        $this->secret = env('USERS_SERVICE_SECRET');

        
    }

    public function listUsers(){
        return $this->performRequest('GET', '/user-list');
    }

    public function createUser($data){
        return $this->performRequest('POST', '/add-user', $data);
    }

    public function editUser($data, $user){
        return $this->performRequest('PUT', "/edit-user/{$user}", $data);
    }

    public function deleteUser($user)
    {
        return $this->performRequest('DELETE', "/delete-user/{$user}");
    }

}