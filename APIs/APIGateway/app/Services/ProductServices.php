<?php

namespace App\Services;

use App\Traits\ConsumeExternalService;

class ProductServices
{
    use ConsumeExternalService;

    /**
     * The base uri to consume authors service
     * @var string
     */
    public $baseUri;

    /**
     * Authorization secret to pass to book api
     * @var string
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = env('USERS_SERVICE_BASE_URL');
        $this->secret = env('USERS_SERVICE_SECRET');

        
    }

    public function listProducts(){
        return $this->performRequest('GET', '/product-list');
    }

    public function createProduct($data){
        return $this->performRequest('POST', '/add-product', $data);
    }

    public function editProduct($data, $user){
        return $this->performRequest('PUT', "/edit-product/{$user}", $data);
    }

    public function deleteProduct($user)
    {
        return $this->performRequest('DELETE', "/delete-product/{$user}");
    }

}