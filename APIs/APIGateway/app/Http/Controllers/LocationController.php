<?php

namespace App\Http\Controllers;

use App\Services\LocationServices;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class LocationController extends Controller
{
    use ApiResponser;

    /**
     * The service to consume the authors micro-service
     * @var LocationServices
     */
    public $locationService;

    public function __construct(LocationServices $locationService)
    {
        $this->locationService = $locationService;
    }


    /**
     * Get Author data
     * @return \Illuminate\Http\JsonResponse
     */
    public function listCountry()
    {
        return $this->successResponse($this->locationService->listCountry());
    }

    public function listState($country)
    {
        return $this->successResponse($this->locationService->listState($country));
    }

    public function listCity($state)
    {
        return $this->successResponse($this->locationService->listCity($state));
    }

}