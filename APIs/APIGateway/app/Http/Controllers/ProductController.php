<?php

namespace App\Http\Controllers;

use App\Services\ProductServices;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class ProductController extends Controller
{
    use ApiResponser;

    /**
     * The service to consume the authors micro-service
     * @var ProductServices
     */
    public $productService;

    public function __construct(ProductServices $productService)
    {
        $this->productService = $productService;
    }


    /**
     * Get Author data
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        return $this->successResponse($this->productService->listProducts());
    }


    /**
     * Save an author data
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return $this->successResponse($this->productService->createProduct($request->all()));
    }



    /**
     * Update a single author data
     * @param Request $request
     * @param $author
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $product)
    {
        return $this->successResponse($this->productService->editProduct($request->all(),$product));
    }


    /**
     * Delete a single author details
     * @param $author
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($product)
    {
        return $this->successResponse($this->productService->deleteProduct($product));
    }
}
