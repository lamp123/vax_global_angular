<?php

namespace App\Http\Controllers;

use App\Services\UserServices;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class UserController extends Controller
{
    use ApiResponser;

    /**
     * The service to consume the authors micro-service
     * @var UserServices
     */
    public $userService;

    public function __construct(UserServices $userService)
    {
        $this->userService = $userService;
    }


    /**
     * Get Author data
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        return $this->successResponse($this->userService->listUsers());
    }


    /**
     * Save an author data
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return $this->successResponse($this->userService->createUser($request->all()));
    }


    /**
     * Show a single author details
     * @param $author
     * @return \Illuminate\Http\JsonResponse
     */
    // public function show($user)
    // {
    //     return $this->successResponse($this->userService->obtainAuthor($user));
    // }


    /**
     * Update a single author data
     * @param Request $request
     * @param $author
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $user)
    {
        return $this->successResponse($this->userService->editUser($request->all(),$user));
    }


    /**
     * Delete a single author details
     * @param $author
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($user)
    {
        return $this->successResponse($this->userService->deleteUser($user));
    }
}
